import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl} from '@angular/forms';
import{Router,Routes} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import{HTTP} from '@ionic-native/http/ngx'
import { ToastController } from '@ionic/angular';
import { Services } from '../services/services';




@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
    register: FormGroup;
  constructor(private service:Services,private router: Router, public http:HttpClient,private nativeHttp: HTTP,private toastController: ToastController) { }
  registerSubmit(){
    const params = {
      'name': this.register.controls.name.value,
      'email': this.register.controls.email.value,
      'password': this.register.controls.password.value,
    };
    this.service.register(params).subscribe((res:any)=>{
      console.log('res',res);
    })
    
  }
  
  async cont(){
   const toast = await this.toastController.create({
    message:'Successfully Registered!',
    duration:3000,
    position:"middle",
   })
   toast.present();
   this.router.navigate(['/signin']);
  }
  sign(){
    this.router.navigate(['/signin']);
  }
  signup(){
   this.router.navigate(['/signup']);

  }
  ngOnInit() {
  this.register = new FormGroup({
    name:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(10)]),
    email: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]),


  });
  }

}
