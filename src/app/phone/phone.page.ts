import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PhoneValidator } from '../validators/phone.validator';
import { CountryPhone } from './country-phone.module';
import { Router } from '@angular/router';
import { Services } from '../services/services';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.page.html',
  styleUrls: ['./phone.page.scss'],
})
export class PhonePage implements OnInit {

  validations_form: FormGroup;
  country_phone_group: FormGroup;

  countries: Array<CountryPhone>;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,private service:Services,
  ) { }

  ngOnInit() {
    //  We just use a few random countries, however, you can use the countries you need by just adding them to this list.
    // also you can use a library to get all the countries from the world.
    this.countries = [
      new CountryPhone('AF', 'Afghanistan'),
      new CountryPhone('AL', 'Albania'),
      new CountryPhone('DZ', 'Algeria'),
      new CountryPhone('AS', 'American Samoa'),
      new CountryPhone('AD', 'Andorra'),
      new CountryPhone('AO', 'Anguilla'),
      new CountryPhone('AI', 'American Samoa'),
      new CountryPhone('AG', 'Antigua and Barbuda'),
      new CountryPhone('AR', 'Argentina'),
      new CountryPhone('AM', 'Armenia'),
      new CountryPhone('AW', 'Aruba'),
      new CountryPhone('AU', 'Australia'),
      new CountryPhone('AT', 'Austria'),
      new CountryPhone('AZ', 'Azerbaijan'),
      new CountryPhone('BS', 'Bahamas'),
      new CountryPhone('BH', 'Bahrain'),
      new CountryPhone('BD', 'Bangladesh'),
      new CountryPhone('BB', 'Barbados'),
      new CountryPhone('BY', 'Belarus'),
      new CountryPhone('BE', 'Belgium'),
      new CountryPhone('BZ', 'Belize'),
      new CountryPhone('BJ', 'Benin'),
      new CountryPhone('BM', 'Bermuda'),
      new CountryPhone('BT', 'Bhutan'),
      new CountryPhone('BO', 'Bolivia'),
      new CountryPhone('BQ', 'Bonaire'),
      new CountryPhone('BA', 'Bosnia and Herzegovina'),
      new CountryPhone('BW', 'Botswana'),
      new CountryPhone('BR', 'Brazil'),
      new CountryPhone('IO', 'British Indian Ocean Territory'),
      new CountryPhone('BN', 'British Brunei Darussalam'),
      new CountryPhone('BG', 'Bulgaria'),
      new CountryPhone('BF', 'Burkina Faso'),
      new CountryPhone('BI', 'Burundi'),
      new CountryPhone('KH', 'Cambodia'),
      new CountryPhone('CM', 'Cameroon'),
      new CountryPhone('CA', 'Canada'),
      new CountryPhone('CV', 'Cape Verde'),
      new CountryPhone('KY', 'Cayman Islands'),
      new CountryPhone('CF', 'Central African Republic'),
      new CountryPhone('CL', 'Chile'),
      new CountryPhone('CN', 'China'),
      new CountryPhone('CO', 'Colombia'),
      new CountryPhone('KM', 'Comoros'),
      new CountryPhone('CR', 'Costa Rica'),
      new CountryPhone('HR', 'Croatia'),
      new CountryPhone('CU', 'Cuba'),
      new CountryPhone('CY', 'Cyprus'),
      new CountryPhone('CZ', 'Czech Republic'),
      new CountryPhone('DK', 'Denmark'),
      new CountryPhone('DM', 'Dominica'),
      new CountryPhone('DO', 'Dominican Republic'),
      new CountryPhone('EC', 'Ecuador'),
      new CountryPhone('EG', 'Egypt'),
      new CountryPhone('SV', 'El Salvador'),
      new CountryPhone('EE', 'Estonia'),
      new CountryPhone('ET', 'Ethiopia'),
      new CountryPhone('FI', 'Finland'),
      new CountryPhone('FR', 'France'),
      new CountryPhone('GF', 'French Guiana'),
      new CountryPhone('PF', 'French Polynesia'),
      new CountryPhone('GA', 'Gabon'),
      new CountryPhone('GM', 'Gambia'),
      new CountryPhone('GE', 'Georgia'),
      new CountryPhone('DE', 'Germany'),
      new CountryPhone('GH', 'Ghana'),
      new CountryPhone('GR', 'Greece'),
      new CountryPhone('GL', 'Greenland'),
      new CountryPhone('GN', 'Grenada'),
      new CountryPhone('GY', 'Guyana'),
      new CountryPhone('HT', 'Haiti'),
      new CountryPhone('HK', 'Hong Kong'),
      new CountryPhone('HU', 'Hungary'),
      new CountryPhone('IS', 'Iceland'),
      new CountryPhone('IN', 'India'),
      new CountryPhone('ID', 'Indonesia'),
      new CountryPhone('IR', 'Iran'),
      new CountryPhone('IQ', 'Iraq'),
      new CountryPhone('IE', 'Ireland'),
      new CountryPhone('IL', 'Israel'),
      new CountryPhone('IT', 'Italy'),
      new CountryPhone('JM', 'Jamaica'),
      new CountryPhone('JP', 'Japan'),
      new CountryPhone('JO', 'Jordan'),
      new CountryPhone('KZ', 'Kazakhstan'),
      new CountryPhone('KE', 'Kenya'),
      new CountryPhone('KR', 'Korea, Republic of'),
      new CountryPhone('KW', 'Kuwait'),
      new CountryPhone('KG', 'Kyrgyzstan'),
      new CountryPhone('LV', 'Latvia'),
      new CountryPhone('LB', 'Lebanon'),
      new CountryPhone('LR', 'Liberia'),
      new CountryPhone('LY', 'Libya'),
      new CountryPhone('LT', 'Lithuania'),
      new CountryPhone('LU', 'Luxembourg'),
      new CountryPhone('MO', 'Macao'),
      new CountryPhone('MK', 'Republic of North Macedonia'),
      new CountryPhone('MG', 'Madagascar'),
      new CountryPhone('MW', 'Malawi'),
      new CountryPhone('MY', 'Malaysia'),
      new CountryPhone('MV', 'Maldives'),
      new CountryPhone('ML', 'Mali'),
      new CountryPhone('MT', 'Malta'),
      new CountryPhone('MX', 'Mexico'),
      new CountryPhone('MD', 'Moldova, Republic of'),
      new CountryPhone('MC', 'Monaco'),
      new CountryPhone('MN', 'Mongolia'),
      new CountryPhone('ME', 'Montenegro'),
      new CountryPhone('MA', 'Morocco'),
      new CountryPhone('NP', 'Nepal'),
      new CountryPhone('NL', 'Netherlands'),
      new CountryPhone('NZ', 'New Zealand'),
      new CountryPhone('NG', 'Nigeria'),
      new CountryPhone('NO', 'Norway'),
      new CountryPhone('PK', 'Pakistan'),
      new CountryPhone('PS', 'Palestine, State of'),
      new CountryPhone('PA', 'Panama'),
      new CountryPhone('PY', 'Paraguay'),
      new CountryPhone('PE', 'Peru'),
      new CountryPhone('PH', 'Philippines'),
      new CountryPhone('PL', 'Poland'),
      new CountryPhone('PT', 'Portugal'),
      new CountryPhone('PR', 'Puerto Rico'),
      new CountryPhone('QA', 'Qatar'),
      new CountryPhone('RO', 'Romania'),
      new CountryPhone('RU', 'Russian Federation'),
      new CountryPhone('WS', 'Samoa'),
      new CountryPhone('SM', 'San Marino'),
      new CountryPhone('SA', 'Saudi Arabia'),
      new CountryPhone('SN', 'Senegal'),
      new CountryPhone('RS', 'Serbia'),
      new CountryPhone('SG', 'Singapore'),
      new CountryPhone('SK', 'Slovakia'),
      new CountryPhone('SI', 'Slovenia'),
      new CountryPhone('SO', 'Somalia'),
      new CountryPhone('ZA', 'South Africa'),
      new CountryPhone('ES', 'Spain'),
      new CountryPhone('LK', 'Sri Lanka'),
      new CountryPhone('SE', 'Sri Sweden'),
      new CountryPhone('CH', 'Switzerland'),
      new CountryPhone('SY', 'Syrian Arab Republic'),
      new CountryPhone('TW', 'Taiwan'),
      new CountryPhone('TJ', 'Tajikistan'),
      new CountryPhone('TZ', 'United Republic of Tanzania	'),
      new CountryPhone('TH', 'Thailand'),
      new CountryPhone('TN', 'Tunisia'),
      new CountryPhone('TR', 'Turkey'),
      new CountryPhone('TC', 'Turkmenistan'),
      new CountryPhone('UG', 'Uganda'),
      new CountryPhone('UA', 'Ukraine'),
      new CountryPhone('AE', 'United Arab Emirates'),
      new CountryPhone('GB', 'United Kingdom'),
      new CountryPhone('US', 'United States'),
      new CountryPhone('UY', 'Uruguay'),
      new CountryPhone('UZ', 'Uzbekistan'),
      new CountryPhone('VE', 'Venezuela'),
      new CountryPhone('YE', 'Yemen'),
      new CountryPhone('ZA', 'Zambia'),


    ];
    let country = new FormControl(this.countries[0], Validators.required);
    let phone = new FormControl('', Validators.compose([
      Validators.required,
      PhoneValidator.validCountryPhone(country)
    ]));
    this.country_phone_group = new FormGroup({
      country: country,
      phone: phone,
     
    });
    this.validations_form = this.formBuilder.group({
      country_phone: this.country_phone_group,
      terms: new FormControl(true, Validators.pattern('true'))
    });
  }
  updatePhoneNumber(){
    const body = {
      phone:this.country_phone_group.controls.phone.value
    }
    this.service.updatePhone(body).subscribe((res:any)=>{
      console.log('res',res);
      this.router.navigate(["/nearbymap"]);
  })
  

}  
  

  validation_messages = {
    'phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'validCountryPhone', message: 'The phone is incorrect for the selected country.' }
       ],
  onSubmit(values){
    console.log(values);
    this.router.navigate(["/user"]);
  }
}
}