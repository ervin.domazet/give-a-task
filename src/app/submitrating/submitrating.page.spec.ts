import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitratingPage } from './submitrating.page';

describe('SubmitratingPage', () => {
  let component: SubmitratingPage;
  let fixture: ComponentFixture<SubmitratingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitratingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitratingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
