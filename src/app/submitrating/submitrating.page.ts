import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormControl } from '@angular/forms';
import { Services } from '../services/services';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

declare var google;
@Component({
  selector: 'app-submitrating',
  templateUrl: './submitrating.page.html',
  styleUrls: ['./submitrating.page.scss'],
})
export class SubmitratingPage implements OnInit, AfterViewInit {
   latitude: any;
   longitude: any;
   rating = new FormControl;
   job_userId;
   public jobusers:object;
   @ViewChild('mapElement') mapNativeElement:ElementRef;
  constructor(private geolocation:Geolocation,private service:Services,  private nvg: Router,private route: ActivatedRoute) { }

  ngOnInit() : void {

  this.route.queryParams.subscribe((res:any)=>{
    console.log('jobuserid',res)
    this.job_userId = res.id;
    this.service.getJobUsers(this.job_userId).subscribe(result =>{
      console.log('results',result)
      this.jobusers = result;
    },
    err =>{
      console.log(err);
      return false;
    });
  })
  
  }
  SubmitRate(){
    const body = {
      job_user_id:this.job_userId,
      user_rating:this.rating.value

    }
    this.service.SubmitRating(body).subscribe(res =>{
      this.jobusers = res;
      console.log('results',res)
    })
    this.nvg.navigate(['/nearbymap']);
  }
  
  ngAfterViewInit(): void {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: {lat: -34.397, lng: 150.644},
        zoom: 16
      });
      const infoWindow = new google.maps.InfoWindow;
      const pos = {
        lat: this.latitude,
        lng: this.longitude
      };
      map.setCenter(pos);
      const icon ={
        url: 'assets/u.png',//image url
        scaledSize: new google.maps.Size(50, 50), //scaled size
      };
      const marker = new google.maps.Marker({
        position: pos,//marker position
        map: map,//map already created
        title:'Hello World',
        icon:icon //custom image
      });
      const contentString = '<div id="content">' +
    '<div id="siteNotice">' +
    '</div>' +
    '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
    '<div id="bodyContent">' +
    '<img src="assets/user.png" width="200">' +
    '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
    'sandstone rock formation in the southern part of the ' +
    'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) ' +
    'south west of the nearest large town, Alice Springs; 450&#160;km ' +
    '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major ' +
    'features of the Uluru - Kata Tjuta National Park. Uluru is ' +
    'sacred to the Pitjantjatjara and Yankunytjatjara, the ' +
    'Aboriginal people of the area. It has many springs, waterholes, ' +
    'rock caves and ancient paintings. Uluru is listed as a World ' +
    'Heritage Site.</p>' +
    '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
    'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
    '(last visited June 22, 2009).</p>' +
    '</div>' +
    '</div>';
    const infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 400
    });
    marker.addListener('click',function(){
      infowindow.open(map,marker);
    });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

}
