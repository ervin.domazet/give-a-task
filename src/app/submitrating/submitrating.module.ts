import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import{IonicRatingModule} from 'ionic4-rating';

import { IonicModule } from '@ionic/angular';

import { SubmitratingPage } from './submitrating.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';

const routes: Routes = [
  {
    path: '',
    component: SubmitratingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  declarations: [SubmitratingPage],
  providers:[Geolocation]
})
export class SubmitratingPageModule {}
