import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import{HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import{ IonicRatingModule} from 'ionic4-rating';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import{HTTP} from '@ionic-native/http/ngx'
import {Camera} from '@ionic-native/camera/ngx';
import { FileTransfer} from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { from } from 'rxjs';
import { Crop } from '@ionic-native/crop/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';



@NgModule({
  declarations: [AppComponent,],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), IonicStorageModule.forRoot(), AppRoutingModule,IonicRatingModule,InternationalPhoneNumberModule,HttpClientModule],
  providers: [
    StatusBar,
    SplashScreen,
    AndroidPermissions,
    LocationAccuracy,
    CallNumber,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HTTP,
    Geolocation,
    NativeGeocoder,
    Camera,
    FileTransfer,
    ImagePicker,
    Crop,
    File,
    
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
