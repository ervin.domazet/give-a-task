import { Component, OnInit } from '@angular/core';
import { Services } from '../services/services';
import { NavController } from '@ionic/angular';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

 
@Component({
  selector: 'app-nearby',
  templateUrl: './nearby.page.html',
  styleUrls: ['./nearby.page.scss'],
})
export class NearbyPage implements OnInit {
  public jobs: any[];
  public jobs1: any[];
  constructor(private service:Services, private navController: NavController, private nvg: Router,) { }
    backbutton(){
      this.nvg.navigate(['/nearbymap']);
    }
  ngOnInit() {
    this.service.getNearbyJobs().subscribe(result =>{
      this.jobs1 = result;
      console.log('joobs1', result)
    },
    err => {
      console.log(err);
      return false;
    });
    
    // this.service.getJobUsers().subscribe(res=>{
    //   this.jobs = res;
    //   console.log('userjobs',res)
    // },
    // err =>{
    //   console.log(err);
    //   return false;
    // });
  }
  test(job:any):void{
    console.log('job veton', job)
    this.nvg.navigate(['/sushiplace'],{queryParams:{id:job.id}},)
  }

}



