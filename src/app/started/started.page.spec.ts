import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartedPage } from './started.page';

describe('StartedPage', () => {
  let component: StartedPage;
  let fixture: ComponentFixture<StartedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
