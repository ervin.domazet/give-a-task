import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-started',
  templateUrl: './started.page.html',
  styleUrls: ['./started.page.scss'],
})
export class StartedPage implements OnInit {

  constructor(private navController: NavController, private nvg: Router,) { }
  backbutton(){
    this.nvg.navigate(['/welcome']);
  }
  Next(){
    this.nvg.navigate(['/signin']);
  }
  ngOnInit() {
  }

}
