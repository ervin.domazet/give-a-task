import { Component, OnInit } from '@angular/core';
import { Services } from '../services/services';
import { NavController } from '@ionic/angular';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-sub-categories',
  templateUrl: './sub-categories.page.html',
  styleUrls: ['./sub-categories.page.scss'],
})
export class SubCategoriesPage implements OnInit {
public subcategories: object;
  constructor(private service:Services, private navController: NavController, private nvg: Router) {}
backbutton(){
  this.nvg.navigate(['/categories-job']);
}
  ngOnInit() {
    this.service.getSubCategories().subscribe(result =>{
      this.subcategories = result;
    },
    err => {
      console.log(err);
      return false;
    });
  }


  test(subcategory: any): void{

    this.nvg.navigate(['/last-stop'], { queryParams: {id: subcategory.id}},) 
  }

}
