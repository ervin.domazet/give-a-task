import { Component, OnInit } from '@angular/core';
import{FormControl, FormGroup, Validators, ValidatorFn, AbstractControl} from '@angular/forms';
import{Router,Routes} from '@angular/router';
import{HttpClient} from '@angular/common/http';
import{Observable} from 'rxjs';
import { ToastController, NavController } from '@ionic/angular';
import { Services } from '../services/services';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {
login:FormGroup
  constructor(private service: Services, private navCtrl: NavController, private router:Router,public http:HttpClient, private toastController: ToastController) { }
  testSubmit(){
    const params = {
      'email': this.login.controls.email.value,
      'password': this.login.controls.password.value
    };

     this.service.login(params).subscribe((res: any) => {
        if(res){
          console.log('res',res);
          if(res.message){
            this.toastUnregister();
            this.navCtrl.navigateRoot("/signup");
          }else{
            localStorage.removeItem("Token");
            localStorage.setItem("Token", res.access_token)
            this.navCtrl.navigateRoot("/phone");
            this.toastSuccsess();
          }
      }
    },
    (err: any) => {
      if(err.status === 404){
        this.toastUnregister();
      }else{
        this.toastFail();
      }
    }

    )

    

  }
  
  async toastSuccsess(){
    const toast = await this.toastController.create({
     message:'Successfully Logged in!',
     duration:3000,
     position:"middle",
    })
    toast.present();
   }

   async toastUnregister(){
    const toast = await this.toastController.create({
     message:'this email address does not exist',
     duration:3000,
     position:"middle",
    })
    toast.present();
   }

   async toastFail(){
    const toast = await this.toastController.create({
     message:'Fatal Error',
     duration:3000,
     position:"middle",
    })
    toast.present();
   }
   sign(){
     this.router.navigate(['/signin']);
   }
   signup(){
    this.router.navigate(['/signup']);

   }

  ngOnInit() {
  this.login = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('',[Validators.required,Validators.minLength(5), Validators.maxLength(10)])
  });
  }

}
