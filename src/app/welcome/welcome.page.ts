import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(private navController: NavController, private nvg: Router,) { }
   start(){
     this.nvg.navigate(['/started']);
   }
   getd(){
     this.nvg.navigate(['/last-stop']);
   }
  ngOnInit() {
  }

}
