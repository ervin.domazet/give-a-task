import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import {FormBuilder, FormGroup,FormControl, Validators} from '@angular/forms';
import { Platform, AlertController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Services } from '../services/services';
import { NavController } from '@ionic/angular';


declare var google;
@Component({
  selector: 'app-driving',
  templateUrl: './driving.page.html',
  styleUrls: ['./driving.page.scss'],
})
export class DrivingPage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  currentMapTrack: any;
  isTracking = false;
  trackedRoute = [];
  previousTracks = [];
  positionSubscription: Subscription;
  job_userId;
  public jobusers:object;
  rating = new FormControl;

  constructor(private plt: Platform, private geolocation:Geolocation,private alertCtrl: AlertController,private storage:Storage,private service:Services,private navController: NavController, private nvg: Router,private route: ActivatedRoute) { }

  ngOnInit()  { 
    
    this.route.queryParams.subscribe((res: any)=> {
      console.log('jobuserid', res)
      this.job_userId = res.id;
      this.service.getJobUsers(this.job_userId).subscribe(result =>{
        console.log('results',result)
        this.jobusers = result;
     
      },
      err =>{
        console.log(err);
        return false;
      });
    })
  }
  Done(jobuser:any):void{
    this.nvg.navigate(['/confirm-pickup'],{queryParams:{id:jobuser.id}},)


  }
  ngAfterContentInit(): void{
    this.plt.ready().then(()=> {
      this.loadHistoricRoutes();

      let mapOptions = {
         zoom: 10,
         mapTypeId : google.maps.MapTypeId.ROADMAP,
         mapTypeControl: false,
         streetViewControl: false,
         fullscreenControl: false
      };
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions); 

      this.geolocation.getCurrentPosition().then(pos =>{
        let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        this.map.setCenter(latLng);
        this.map.setZoom(15);
      });
    });
  }
  loadHistoricRoutes(){
    this.storage.get('routes').then(data =>{
      if(data){
        this.previousTracks = data;
      }
    })
  }
  startTracking(){
    this.isTracking = true;
    this.trackedRoute = [];
 
    this.positionSubscription = this.geolocation.watchPosition()
      .pipe(
        filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {
          this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
          this.redrawPath(this.trackedRoute);
        }, 0);
      });


  }
  redrawPath(path){
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }
 
    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: '#ff00ff',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });
      this.currentMapTrack.setMap(this.map);
    }

  }
  stopTracking(){
    let newRoute = {finished: new Date().getTime(), path: this.trackedRoute};
    this.previousTracks.push(newRoute);
    this.storage.set('routes', this.previousTracks);

    this.isTracking = false;
    this.positionSubscription.unsubscribe();
    this.currentMapTrack.setMap(null);
  }

  showHistoryRoute(route){
    this.redrawPath(route);
  }

  clearPreviousTracks(){
    this.previousTracks = [];
  }
}
