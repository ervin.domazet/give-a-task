import { Component, OnInit } from '@angular/core';
import { Services } from '../services/services';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-categories-job',
  templateUrl: './categories-job.page.html',
  styleUrls: ['./categories-job.page.scss'],
})
export class CategoriesJobPage implements OnInit {
public categories:object;
categoriesData: any;
  constructor(private service:Services,private navController: NavController, private nvg: Router) { }
  backbutton(){
    this.nvg.navigate(['/nearbymap']);
  }
  ngOnInit() {
 this.service.getCategories().subscribe(res =>{
   this.categories = res;
   this.categoriesData = res.data
 },
 err => {
   console.log(err);
   return false;
 });
  }

}
