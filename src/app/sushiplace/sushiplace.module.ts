import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SushiplacePage } from './sushiplace.page';
import{ IonicRatingModule } from 'ionic4-rating';
import { Geolocation } from '@ionic-native/geolocation/ngx';
const routes: Routes = [
  {
    path: '',
    component: SushiplacePage
  }
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule

  ],
  declarations: [SushiplacePage],
  providers:[Geolocation]
})
export class SushiplacePageModule {}
