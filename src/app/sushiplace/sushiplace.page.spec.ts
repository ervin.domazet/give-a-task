import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SushiplacePage } from './sushiplace.page';

describe('SushiplacePage', () => {
  let component: SushiplacePage;
  let fixture: ComponentFixture<SushiplacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SushiplacePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SushiplacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
