import { Component, OnInit } from '@angular/core';
import {NavController, LoadingController , ToastController, } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject, } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import swal from 'sweetalert';
import { Services } from '../services/services';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Crop } from '@ionic-native/crop/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder,NativeGeocoderOptions,NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';


@Component({
  selector: 'app-last-stop',
  templateUrl: './last-stop.page.html',
  styleUrls: ['./last-stop.page.scss'],
})
export class LastStopPage implements OnInit {
  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy:number;
  geoAddress: string;
  watchLocationUpdates:any;
  loading:any;
  isWatching:boolean;
  //Geocoder configuration
  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  croppedImagepath = "";
  isLoading = false;
  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };
  jobForm: FormGroup
  responsefromapi: any;
  fileUrl: any = null;
  respData: any;
  sub_categoryId;
  name = new FormControl
  constructor(private service:Services, private navController: NavController, private nvg: Router, private _formBuilder: FormBuilder, private route: ActivatedRoute,public navctrl: NavController, private camera: Camera, private transfer: FileTransfer, private file: File, public loadingController: LoadingController, public toastCtrl: ToastController, public actionSheetController: ActionSheetController, private crop: Crop,
    private imagePicker: ImagePicker,  
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) {
  
    }
   //Get current coordinates of device
 getGeolocation(){
  this.geolocation.getCurrentPosition().then((resp) => {
    this.geoLatitude = resp.coords.latitude;
    this.geoLongitude = resp.coords.longitude;
    this.geoAccuracy = resp.coords.accuracy;
    this.getGeoencoder(this.geoLatitude,this.geoLongitude);
   }).catch((error) => {
     alert('Error getting location'+ JSON.stringify(error));
   });
}
//geocoder method to fetch address from coordinates passed as arguments
getGeoencoder(latitude,longitude){
  this.nativeGeocoder.reverseGeocode(latitude, longitude, this.geoencoderOptions)
  .then((result: NativeGeocoderResult[]) => {
    this.geoAddress = this.generateAddress(result[0]);
  })
  .catch((error: any) => {
    alert('Error getting location'+ JSON.stringify(error));
  });
}
 //Return Comma saperated address
 generateAddress(addressObj){
  let obj = [];
  let address = "";
  for (let key in addressObj) {
    obj.push(addressObj[key]);
  }
  obj.reverse();
  for (let val in obj) {
    if(obj[val].length)
    address += obj[val]+', ';
  }
return address.slice(0, -2);
}
  Backbutton(){
    this.nvg.navigate(['/sub-categories']);

  }
  cancel(){
    this.nvg.navigate(['/sub-categories']);
  }
  UploadPhoto(){

    this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then((results) => {
      for (let i = 0; i < results.length; i++) {
          console.log('Image URI: ' + results[i]);
          this.crop.crop(results[i], { quality: 100 })
            .then(
              newImage => {
                console.log('new image path is: ' + newImage);
                const fileTransfer: FileTransferObject = this.transfer.create();
                const uploadOpts: FileUploadOptions = {
                   fileKey: 'file',
                   fileName: newImage.substr(newImage.lastIndexOf('/') + 1)
                };

                fileTransfer.upload(newImage, 'http://localhost/give-a-task-backend/public/api/job', uploadOpts)
                 .then((data) => {
                   console.log(data);
                   this.respData = JSON.parse(data.response);
                   console.log(this.respData);
                   this.fileUrl = this.respData.fileUrl;
                 }, (err) => {
                   console.log(err);
                 });
              },
              error => console.error('Error cropping image', error)
            );
      }
    }, (err) => { console.log(err); });
  }
  
 
  

  ngOnInit() 
  {
    this.jobForm = this._formBuilder.group({
      description: '',
      price: 0,
      image_id:3,

    })
    this.route.queryParams.subscribe((res: any)=> {
      this.sub_categoryId = res.id;
    })

   }

   save(){
     
    const body = {
      name: this.name.value,
      description: this.jobForm.controls.description.value,
      price: this.jobForm.controls.price.value,
      subcategory_id: this.sub_categoryId
    }

    this.service.createAJob(body).subscribe((res:any)=>{
      console.log('res',res);
    })
   }
   
 
}

