import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastStopPage } from './last-stop.page';

describe('LastStopPage', () => {
  let component: LastStopPage;
  let fixture: ComponentFixture<LastStopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastStopPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastStopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
