import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelJobReasonPage } from './cancel-job-reason.page';

describe('CancelJobReasonPage', () => {
  let component: CancelJobReasonPage;
  let fixture: ComponentFixture<CancelJobReasonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelJobReasonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelJobReasonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
