import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Job } from '../models/job.model'

@Injectable({
  providedIn: 'root'
})
export class Services {
  
  token = localStorage.getItem('Token');

  headers = new HttpHeaders({
    'Content-Type': 'application/json',

  });

  
  headersAuth = new HttpHeaders({
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${this.token}`
  });
  
  constructor(private http: HttpClient) { }

  getSubCategories(){
    let options = { headers: this.headersAuth };
    return this.http.get('http://localhost/give-a-task-backend/public/api/subcategories?id=1',options).pipe(map((response: any) => response));
  }
register(params){
  let options = { headers: this.headers };
  return this.http.post('http://localhost/give-a-task-backend/public/api/register', params).pipe(map((response: any) => response));

}
  login(params){
    let options = { headers: this.headers };
    return this.http.post('http://localhost/give-a-task-backend/public/api/login', params).pipe(map((response: any) => response));
  }
  getCategories(){
    let options = {headers:this.headersAuth};
   return this.http.get('http://localhost/give-a-task-backend/public/api/categories',options).pipe(map((response: any) => response));
  }
  updatePhone(params){
    let options = {headers:this.headersAuth};
    let postData = {
      phone:params.phone
    }
    console.log('params',postData)
    return this.http.post('http://localhost/give-a-task-backend/public/api/user/update',postData,options).pipe(map((response:any)=>response));
  }
  createAJob(params){
    
    let options = {headers:this.headersAuth};
    let postData = {
      name: params.name,
      description:params.description,
      subcategory_id: params.subcategory_id,
      price:params.price,
      image_id:params.image_id
    }
    console.log('params', postData)
    return this.http.post('http://localhost/give-a-task-backend/public/api/job',postData,options).pipe(map((response:any)=>response));
  }
  getNearbyJobs(){
    let options = {headers:this.headersAuth};
    return this.http.get('http://localhost/give-a-task-backend/public/api/job/all',options).pipe(map((response: any) => response));

  }
  getJobUsers(id: number){
    let options = {headers:this.headersAuth};
    return this.http.get('http://localhost/give-a-task-backend/public/api/job/getUserJob?id='+id, options).pipe(map((response:any) =>response));
  }
  acceptJob(params){
let options = {headers:this.headersAuth};
let postData = {
  job_user_id:params.job_user_id,
}
console.log('params', postData)

return this.http.put('http://localhost/give-a-task-backend/public/api/job/accept',postData,options).pipe(map((response:any) =>response));

  }
  CancelJob(params){
    let options = {headers:this.headersAuth};
    let postData = {
      job_user_id:params.job_user_id,
      cancel_reason:params.cancel_reason
    }
    console.log('params',postData)
    return this.http.post('http://localhost/give-a-task-backend/public/api/user/cancel',postData,options).pipe(map((response:any) =>response));
  }

getJobDetails(id: number){
  let options = {headers:this.headersAuth};
  return this.http.get('http://localhost/give-a-task-backend/public/api/job?id='+id,options).pipe(map((response:any)=>response));
}
SubmitRating(params){
  let options = {headers:this.headersAuth};
  let postData = {
    job_user_id:params.job_user_id,
    user_rating:params.user_rating
    
  }
  console.log('params',postData)
  return this.http.post('http://localhost/give-a-task-backend/public/api/job/rating',postData,options).pipe(map((response:any)=>response));
}
  
}
