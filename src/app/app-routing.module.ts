import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
   { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule' },
  { path: 'started', loadChildren: './started/started.module#StartedPageModule' },
  { path: 'signin', loadChildren: './signin/signin.module#SigninPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'phone', loadChildren: './phone/phone.module#PhonePageModule' },
  { path: 'ontrip', loadChildren: './ontrip/ontrip.module#OntripPageModule' },
  { path: 'nearbymap', loadChildren: './nearbymap/nearbymap.module#NearbymapPageModule' },
  { path: 'sub-categories', loadChildren: './sub-categories/sub-categories.module#SubCategoriesPageModule' },
  { path: 'last-stop', loadChildren: './last-stop/last-stop.module#LastStopPageModule' },
  { path: 'sushiplace', loadChildren: './sushiplace/sushiplace.module#SushiplacePageModule' },
  { path: 'nearby', loadChildren: './nearby/nearby.module#NearbyPageModule' },
  { path: 'categories-job', loadChildren: './categories-job/categories-job.module#CategoriesJobPageModule' },
  { path: 'searchresult', loadChildren: './searchresult/searchresult.module#SearchresultPageModule' },
  { path: 'driving', loadChildren: './driving/driving.module#DrivingPageModule' },
  { path: 'submitrating', loadChildren: './submitrating/submitrating.module#SubmitratingPageModule' },
  { path: 'confirm-pickup', loadChildren: './confirm-pickup/confirm-pickup.module#ConfirmPickupPageModule' },
  { path: 'cancel-job-reason', loadChildren: './cancel-job-reason/cancel-job-reason.module#CancelJobReasonPageModule' },
  { path: 'publish/:priceid', loadChildren: './publish/publish.module#PublishPageModule' },
  { path: 'searchbar', loadChildren: './searchbar/searchbar.module#SomesearchPageModule' },
  { path: 'get-direction', loadChildren: './get-direction/get-direction.module#GetDirectionPageModule' },







];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
