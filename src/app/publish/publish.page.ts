import { Component, OnInit } from '@angular/core';
import { Services } from '../services/services';
import { NavController } from '@ionic/angular';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import{ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-publish',
  templateUrl: './publish.page.html',
  styleUrls: ['./publish.page.scss'],
})
export class PublishPage implements OnInit {
 price = null;
  constructor(private service:Services, private navController: NavController, private nvg: Router,private activatedRoute:ActivatedRoute) { }
  Finish(){
    this.nvg.navigate(['/nearbymap']);
  }
  ngOnInit() {
   this.price = this.activatedRoute.snapshot.paramMap.get('priceid');
  }

}
