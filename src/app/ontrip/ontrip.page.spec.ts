import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OntripPage } from './ontrip.page';

describe('OntripPage', () => {
  let component: OntripPage;
  let fixture: ComponentFixture<OntripPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OntripPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OntripPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
