import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup,FormControl, Validators} from '@angular/forms';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { Services } from '../services/services';
import { Router } from '@angular/router';

declare var google;
@Component({
  selector: 'ontrip',
  templateUrl: './ontrip.page.html',
  styleUrls: ['./ontrip.page.scss'],
})
export class OntripPage implements OnInit, AfterViewInit {
  @ViewChild('mapElement') mapNativeElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  directionForm: FormGroup;
  currentLocation: any = {
    lat: 0,
    lng: 0
  };
  job_userId;
  public jobusers:object;
  rating = new FormControl;
  constructor(private fb: FormBuilder, private geolocation: Geolocation,private service:Services, private nvg: Router,private route: ActivatedRoute) {
    this.createDirectionForm();
  }

  ngOnInit() {
    
    this.route.queryParams.subscribe((res:any)=>{
      console.log('jobuserid',res)
      this.job_userId = res.id;
      this.service.getJobUsers(this.job_userId).subscribe(result =>{
        console.log('results',result)
        this.jobusers = result;
      },
      err =>{
        console.log(err);
        return false;
      });

    })
  }
  Drive(jobuser:any):void{
this.nvg.navigate(['/driving'],{queryParams:{id:jobuser.id}},)
  }

  createDirectionForm() {
    this.directionForm = this.fb.group({
      destination: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.currentLocation.lat = resp.coords.latitude;
      this.currentLocation.lng = resp.coords.longitude;
    });
    const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
      zoom: 7,
      center: {lat: 41.85, lng: -87.65}
    });
    this.directionsDisplay.setMap(map);
  }

  calculateAndDisplayRoute(formValues) {
    const that = this;
    this.directionsService.route({
      origin: this.currentLocation,
      destination: formValues.destination,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        that.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

}
