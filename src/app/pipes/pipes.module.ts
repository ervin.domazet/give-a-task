import { NgModule } from '@angular/core';
import { FiltroPipe } from './filtro.pipe';
import { SearchPipe } from './search.pipe';
@NgModule({
  declarations: [FiltroPipe, SearchPipe],
  exports:[FiltroPipe,SearchPipe]

})
export class PipesModule { }
