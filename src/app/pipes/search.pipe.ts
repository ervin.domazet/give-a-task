import { Pipe, PipeTransform } from '@angular/core';
import { Job } from '../models/job.model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {


  transform( jobs: Job[], texto: string ): Job[] {

    if ( texto.length === 0 ) { return jobs; }

    texto = texto.toLocaleLowerCase();

    return jobs.filter( job => {
      return job.name.toLocaleLowerCase().includes(texto)
             || job.description.toLocaleLowerCase().includes(texto)
     });

  }

}
